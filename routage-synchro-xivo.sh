#!/bin/bash
# -*- coding: UTF-8 -*-
# Copyright (C) 2012  Avencall
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,

#
#Script Configuration
#-------------------

#Files
TMP_FILE_XIVO=/tmp/extensions_xivo.csv
TMP_FILE_ROUTAGE=/tmp/extensions_routage.csv
TMP_FILE_SQL=/tmp/extensions_request.sql
LOG_FILE=/var/log/routage-synchro-xivo/routage-synchro-xivo.log

#Database Configuration
DATABASE_SRC_USER=icx
DATABASE_SRC_NAME=icx
DATABASE_SRC_PASSWORD=icx
DATABASE_SRC_HOST=127.0.0.1
DATABASE_SRC_PORT=5432


DATABASE_DST_USER=icx
DATABASE_DST_NAME=asterisk
DATABASE_DST_PASSWORD=icx
DATABASE_DST_PORT=5432
#
#Functions
#----------

function logit {
	echo "[`date`] - ${*}" >> $LOG_FILE
	echo "[`date`] - ${*}"
}

function psql_request {
	#Set psql password
	PGPASSWORD=$4
	export PGPASSWORD
	#psql -h host -p port -u username -d database option1 option2
	psql -h $1 -p $2  -U $3 -d $5 -q $6 "$7"
	if [ "$?" = "0" ]; # test success of psql request
	then
        	logit "Request sql success"
	else
        	logit "Error: Problem with sql request"
	        exit 1
	fi


}

#export exten for a specific context in a csv file
#Args: Context
function export_exten_for_specific_context {
 	
	logit "Export exten of routage for context $1"
	psql_request $DATABASE_SRC_HOST $DATABASE_SRC_PORT $DATABASE_SRC_USER $DATABASE_SRC_PASSWORD $DATABASE_SRC_NAME "-c" "\COPY (SELECT digits FROM route where context like '"$1"' and comment like 'auto' order by digits) TO '"$TMP_FILE_ROUTAGE"' CSV DELIMITER ',';"
}


#export exten on en specific xivo
##No export xivo-features exten or exten like _XXXX
#Args: Xivo Host
function export_exten_from_specific_xivo {
	logit "Export exten of xivo $1"
        psql_request $1 $DATABASE_DST_PORT $DATABASE_DST_USER $DATABASE_DST_PASSWORD $DATABASE_DST_NAME "-c" "\COPY (SELECT distinct exten FROM extensions where type in ('queue','group','user','meetme','incall') and commented=0 order by exten) TO '"$TMP_FILE_XIVO"' CSV DELIMITER ',';"
}

#
#Execute sql file to update the table route
function execute_update_query {
	logit "Update exten for xivo"
	psql_request $DATABASE_SRC_HOST $DATABASE_SRC_PORT $DATABASE_SRC_USER $DATABASE_SRC_PASSWORD $DATABASE_SRC_NAME "-f" "$TMP_FILE_SQL"
}

#Check the old exten and generate sql query to delete it
#Args: context
function generate_delete_query_for_old_exten {
	CONTEXT=$1
	#Check if exten must be deleted
	result=$(comm --nocheck-order -13 $TMP_FILE_XIVO $TMP_FILE_ROUTAGE)
	for i in $result;
	do
	       logit "Exten $i with context "$CONTEXT" must be deleted"
	        echo "delete from route where digits like '"$i"' and context like '"$CONTEXT"';" >> $TMP_FILE_SQL;
	done

}

#Check the new exten and generate sql query to add it
#Args: context
function generate_insert_query_for_new_exten {
	CONTEXT=$1
	##check if exten must be added
	result=$(comm --nocheck-order -23 $TMP_FILE_XIVO $TMP_FILE_ROUTAGE)
	INSERT_REQUEST_START="INSERT INTO route (digits, prefix, regexp, target, subroutine, context, comment) VALUES"
	for i in $result;
	do
		logit "Exten $i with context "$CONTEXT" must be added"
		echo "INSERT INTO route (digits, prefix, regexp, target, subroutine, context, comment) VALUES ('"$i"','false', DEFAULT, DEFAULT, DEFAULT,'"$CONTEXT"', 'auto');" >> $TMP_FILE_SQL;
	done;
}

function synchro_xivo {
	XIVO_NAME=$1
	XIVO_CONTEXT=$2
	XIVO_HOST=$3
	logit "starting synchronisation of xivo : $XIVO_NAME"

	export_exten_for_specific_context $XIVO_CONTEXT
	export_exten_from_specific_xivo $XIVO_HOST
	
	echo "--- SQl request for update" > $TMP_FILE_SQL;
	generate_delete_query_for_old_exten $XIVO_CONTEXT
	generate_insert_query_for_new_exten $XIVO_CONTEXT
	execute_update_query
	
	logit "end synchronisation of xivo : $XIVO_NAME"
}


#
#Script Execution
#---------------

#syntax
#synchro_xivo "Name of xivo" "Context to route" "IP of XiVO"

#reset  PGPASSWORD
PGPASSWORD=""
export PGPASSWORD

##End
